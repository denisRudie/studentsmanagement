package ru.university.studentsmanagementservice.adapters.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.university.studentsmanagementservice.domain.Group;
import ru.university.studentsmanagementservice.domain.persistentids.GroupId;

@Repository
public interface GroupJpaRepository extends JpaRepository<Group, GroupId> {

}
