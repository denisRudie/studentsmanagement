package ru.university.studentsmanagementservice.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.university.studentsmanagementservice.adapters.database.StudentsJpaRepository;
import ru.university.studentsmanagementservice.domain.Student;
import ru.university.studentsmanagementservice.domain.persistentids.StudentId;

@Service
@Slf4j
public class StudentsService {
    private final StudentsJpaRepository repository;

    public StudentsService(StudentsJpaRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public Student createOrUpdate(Student student) {
        return repository.save(student);
    }

    public Student get(StudentId studentId) {
        return repository.getById(studentId);
    }
}
