package ru.university.studentsmanagementservice.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.university.studentsmanagementservice.domain.persistentids.PersonId;
import ru.university.studentsmanagementservice.domain.persistentids.StudentId;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Builder
@Table(name = "student")
@EqualsAndHashCode(of = "id", callSuper = false)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Student {

    @EmbeddedId
    @AttributeOverride(name = "value", column = @Column(name = "id"))
    private StudentId id;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "person_id"))
    private PersonId personId;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @Embedded
    private Specialty specialty;

    public void linkToGroup(Group group) {
        this.group = group;
        group.addStudent(this);
    }
}
