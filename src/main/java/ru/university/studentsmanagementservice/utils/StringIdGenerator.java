package ru.university.studentsmanagementservice.utils;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StringIdGenerator {

    public static String newId() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
