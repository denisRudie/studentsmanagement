package ru.university.studentsmanagementservice.utils;

public interface StringValueWrapper {
    String asString();
}
