package ru.university.studentsmanagementservice.adapters.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.university.studentsmanagementservice.domain.Student;
import ru.university.studentsmanagementservice.domain.persistentids.StudentId;

@Repository
public interface StudentsJpaRepository extends JpaRepository<Student, StudentId> {

}
