package ru.university.studentsmanagementservice.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.university.studentsmanagementservice.adapters.database.GroupJpaRepository;
import ru.university.studentsmanagementservice.domain.Group;
import ru.university.studentsmanagementservice.domain.persistentids.GroupId;

@Service
@Slf4j
public class GroupService {
    private final GroupJpaRepository repository;

    public GroupService(GroupJpaRepository repository) {
        this.repository = repository;
    }

    public Group getGroup(GroupId groupId) {
        return repository.getById(groupId);
    }

    @Transactional
    public Group createOrUpdateGroup(Group group) {
        return repository.save(group);
    }

    public void deleteGroup(Group group) {
        repository.delete(group);
    }
}
