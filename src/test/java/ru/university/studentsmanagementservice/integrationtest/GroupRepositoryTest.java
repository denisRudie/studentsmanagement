package ru.university.studentsmanagementservice.integrationtest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.university.studentsmanagementservice.app.GroupService;
import ru.university.studentsmanagementservice.app.StudentsService;
import ru.university.studentsmanagementservice.config.DatabaseTestConfig;
import ru.university.studentsmanagementservice.domain.Group;
import ru.university.studentsmanagementservice.domain.Specialty;
import ru.university.studentsmanagementservice.domain.Student;
import ru.university.studentsmanagementservice.domain.persistentids.GroupId;
import ru.university.studentsmanagementservice.domain.persistentids.PersonId;
import ru.university.studentsmanagementservice.domain.persistentids.StudentId;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional
public class GroupRepositoryTest extends DatabaseTestConfig {

    @Autowired
    private GroupService groupService;

    @Autowired
    private StudentsService studentsService;

    @Test
    @DisplayName("Creating and getting group by id")
    public void groupRepositoryShouldSaveEntity() {
        GroupId groupId = GroupId.newId();

        Group group = Group.builder()
                .id(groupId)
                .learningStartDate(LocalDate.now())
                .learningEndDate(LocalDate.of(2024, 7, 30))
                .build();

        groupService.createOrUpdateGroup(group);

        Group loadedGroup = groupService.getGroup(groupId);

        assertThat(loadedGroup).isEqualTo(group);
    }

    @Test
    @DisplayName("Creating and getting student by id")
    public void studentsRepositoryShouldSaveEntity() {
        StudentId studentId = StudentId.newId();

        Student student = Student.builder()
                .id(studentId)
                .personId(PersonId.newId())
                .specialty(new Specialty("12345", "Политология"))
                .build();

        studentsService.createOrUpdate(student);

        Student loadedStudent = studentsService.get(studentId);

        assertThat(loadedStudent).isEqualTo(student);
    }
}
