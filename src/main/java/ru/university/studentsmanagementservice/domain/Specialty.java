package ru.university.studentsmanagementservice.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Specialty {

    @Column(name = "specialty_code")
    private String code;

    @Column(name = "specialty_name")
    private String name;
}
