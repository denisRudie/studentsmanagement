package ru.university.studentsmanagementservice.domain.persistentids;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.university.studentsmanagementservice.utils.GenericPersistentId;
import ru.university.studentsmanagementservice.utils.StringIdGenerator;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GroupId extends GenericPersistentId {
    public GroupId(String value) {
        super(value);
    }

    public static GroupId newId() {
        return of(StringIdGenerator.newId());
    }

    @JsonCreator
    public static GroupId of(String value) {
        return new GroupId(value);
    }

    public static GroupId of(GenericPersistentId id) {
        return GroupId.of(id.asString());
    }
}
