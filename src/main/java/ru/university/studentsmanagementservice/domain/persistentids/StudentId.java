package ru.university.studentsmanagementservice.domain.persistentids;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.university.studentsmanagementservice.utils.GenericPersistentId;
import ru.university.studentsmanagementservice.utils.StringIdGenerator;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentId extends GenericPersistentId {
    public StudentId(String value) {
        super(value);
    }

    public static StudentId newId() {
        return of(StringIdGenerator.newId());
    }

    @JsonCreator
    public static StudentId of(String value) {
        return new StudentId(value);
    }

    public static StudentId of(GenericPersistentId id) {
        return StudentId.of(id.asString());
    }
}
