package ru.university.studentsmanagementservice.domain;

import lombok.Builder;
import lombok.Data;
import ru.university.studentsmanagementservice.domain.persistentids.PersonId;

@Data
@Builder
public class Person {

    private PersonId id;

    private String lastName;
    private String firstName;
    private String middleName;
    private String fullName;
}
