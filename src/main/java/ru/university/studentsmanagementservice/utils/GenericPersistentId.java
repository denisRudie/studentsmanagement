package ru.university.studentsmanagementservice.utils;

import com.fasterxml.jackson.annotation.JsonValue;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public class GenericPersistentId implements Serializable, StringValueWrapper {
    protected String value;

    public GenericPersistentId(String value) {
        this.value = value;
    }

    public GenericPersistentId() {
    }

    public boolean notSet() {
        return this.value == null || this.value.isEmpty();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            GenericPersistentId genericId = (GenericPersistentId)o;
            return Objects.equals(this.value, genericId.value);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return this.value != null ? this.value.hashCode() : 0;
    }

    @JsonValue
    public String asString() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }
}
