package ru.university.studentsmanagementservice.domain.persistentids;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.university.studentsmanagementservice.utils.GenericPersistentId;
import ru.university.studentsmanagementservice.utils.StringIdGenerator;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PersonId extends GenericPersistentId {
    public PersonId(String value) {
        super(value);
    }

    public static PersonId newId() {
        return of(StringIdGenerator.newId());
    }

    @JsonCreator
    public static PersonId of(String value) {
        return new PersonId(value);
    }

    public static PersonId of(GenericPersistentId id) {
        return PersonId.of(id.asString());
    }
}
